/*
 * Copyright 2016 Andrew McGuiness
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at:
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ECS.ExampleComponents;

import ECS.Component;
import ECS.GameObject;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class SpriteRenderer extends Component {
	private Sprite sprite;

	public SpriteRenderer( GameObject owner, Sprite sprite){
		super(owner);
		this.sprite = new Sprite( sprite );
		this.updates = true;
	}

	@Override
	public void render( SpriteBatch batch ) {
		sprite.draw( batch );
	}

	public void changeSprite( Sprite sprite ){
		this.sprite = sprite;
	}

	@Override
	public void update( float deltaTime ) {
		super.update( deltaTime );

		Vector2 pos = new Vector2( owner.getLocalPosition() );
		sprite.setPosition( pos.x - sprite.getWidth()/2f, pos.y - sprite.getHeight()/2f );
	}

	public Sprite getSprite(){
		return sprite;
	}
}
