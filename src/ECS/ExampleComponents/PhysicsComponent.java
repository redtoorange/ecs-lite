/*
 * Copyright 2016 Andrew McGuiness
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at:
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ECS.ExampleComponents;

import ECS.Component;
import ECS.GameObject;
import ECS.PhysicsWorld;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

public class PhysicsComponent extends Component {
	private Body body;
	private BodyDef bDef;
	private FixtureDef fDef;
	private float speed = 1f;

	public PhysicsComponent( GameObject owner, PhysicsWorld world, float height, float width ) {
		super( owner );

		PolygonShape shape = new PolygonShape( );
		shape.setAsBox( height / 2f, width / 2f );

		bDef = new BodyDef( );
		bDef.type = BodyDef.BodyType.DynamicBody;
		bDef.position.set( owner.getLocalPosition( ) );
		bDef.fixedRotation = true;
		bDef.linearDamping = 10;

		fDef = new FixtureDef( );
		fDef.shape = shape;
		fDef.density = 1;
		fDef.friction = 1;
		fDef.restitution = 0;

		world.addInstance( this );

		this.renders = false;
		this.updates = true;
	}

	public void created( World world ) {
		System.out.println( "Creating body" );
		body = world.createBody( bDef );
		body.createFixture( fDef );
		fDef.shape.dispose( );
	}

	public void destroyed( World world ) {
		world.destroyBody( body );
		body = null;
	}

	@Override
	public void update( float deltaTime ) {
		super.update( deltaTime );
		owner.setLocalPosition( body.getPosition( ) );
	}

	public void addForce( Vector2 force ) {
		if( body != null ) body.applyForce( force, body.getPosition( ), true );
	}

	public void addImpulse( Vector2 impulse ) {
		if( body != null ) body.applyLinearImpulse( impulse, body.getPosition( ), true );
	}

	public void setLocation( Vector2 location){
		body.setTransform( location, 0 );
	}
}
