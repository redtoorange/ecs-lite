/*
 * Copyright 2016 Andrew McGuiness
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at:
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ECS.ExampleComponents;

import ECS.Component;
import ECS.GameObject;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class ClickComponent extends Component {
	private float width = 0;
	private float height = 0;
	private Camera camera;
	private boolean pressed = false;
	private Vector3 lastClickLocation = new Vector3(  );

	public ClickComponent( GameObject owner, Camera camera, float width, float height ) {
		super( owner );

		this.renders = false;
		this.updates = true;

		this.width = width;
		this.height = height;
		this.camera = camera;
	}

	@Override
	public void update( float deltaTime ) {
		super.update( deltaTime );

		if( !pressed ) {
			if( Gdx.input.isButtonPressed( Input.Buttons.LEFT ) ) registerLeftClick( );
			if( Gdx.input.isButtonPressed( Input.Buttons.RIGHT ) ) registerRightClick( );
		}

		if( pressed && !Gdx.input.isTouched( ) ) pressed = false;
	}


	protected void registerLeftClick() {
		pressed = true;
		lastClickLocation = camera.unproject( new Vector3( Gdx.input.getX( ), Gdx.input.getY( ), 0 ) );
		if( owner instanceof ClickComponentListener ) ( (ClickComponentListener) owner ).leftClick( );
	}

	protected void registerRightClick() {
		pressed = true;
		lastClickLocation = camera.unproject( new Vector3( Gdx.input.getX( ), Gdx.input.getY( ), 0 ) );
		if( owner instanceof ClickComponentListener ) ( (ClickComponentListener) owner ).rightClick( );
	}

	public boolean mouseInsideBounds() {
		Rectangle bounds = new Rectangle( owner.getLocalPosition( ).x - width / 2, owner.getLocalPosition( ).y - height / 2, width, height );
		Vector3 mousePos = new Vector3( Gdx.input.getX( ), Gdx.input.getY( ), 0 );
		mousePos = camera.unproject( mousePos );

		return bounds.contains( mousePos.x, mousePos.y );
	}

	public Vector2 getLastClickLocation(){
		return new Vector2( lastClickLocation.x, lastClickLocation.y );
	}
}
