/*
 * Copyright 2016 Andrew McGuiness
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at:
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ECS.ExampleComponents;

import ECS.Component;
import ECS.GameObject;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;

public class InputComponent extends Component {
	private float speed = 5.0f;
	private Vector2 input = new Vector2(  );

	public InputComponent( GameObject owner){
		super(owner);
		this.renders = false;
		this.updates = true;
	}

	@Override
	public void update( float deltaTime ) {
		input = new Vector2( 0, 0 );

		if( Gdx.input.isKeyPressed( Input.Keys.W ))
			input.y += 1;
		if( Gdx.input.isKeyPressed( Input.Keys.A ))
			input.x -= 1;
		if( Gdx.input.isKeyPressed( Input.Keys.S ))
			input.y -= 1;
		if( Gdx.input.isKeyPressed( Input.Keys.D ))
			input.x += 1;
	}

	public Vector2 getInput(){
		return input;
	}
}
