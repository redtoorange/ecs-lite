/*
 * Copyright 2016 Andrew McGuiness
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at:
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ECS;

/***
 *		GameObject.java - A complex object that has both child GameObjects and attached Components.  Children should
 *			only be used if objects are logically linked.  An example might be to have "Bullets" childed under a "Gun."
 *			The bag of components provides functionality to the GameObject.  The GameObject to implement minimal code of
 *			it's own.  getComponent() can be used to get the first attached component that matches the type and
 *			findComponent() will seach this component tree, and all the trees of all children until the first Object
 *			matching the type is found.
 *
 *        @author Andrew M.
 *  	@version 11/22/2016
 */

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public abstract class GameObject implements Comparable {
	protected Vector2 worldPosition = new Vector2( 0, 0 );
	protected Vector2 localOffset = new Vector2( 0, 0 );

	protected int zLevel = 0;

	protected String name;
	protected GameObject parent = null;

	protected Array<GameObject> children = new Array<GameObject>( );
	protected Array<Component> components = new Array<Component>( );

	//Used for the rendering zLevel
	@Override
	public int compareTo( Object o ) {
		return this.zLevel - ( (GameObject) o ).zLevel;
	}

	public GameObject( String name ) {
		this.name = name;
	}

	//not sure how to implement since all components need a reference to a GameObject on creation
//	public GameObject( String name, Component[] startingComponents ) {
//		this.name = name;
//		for ( Component c : startingComponents ) {
//			components.add( c );
//		}
//	}

	//update both bags
	public void update( float deltaTime ) {
		updateLocalPosition( );

		for ( int i = 0; i < components.size; i++ )
			if( components.get( i ).getUpdates( ) ) components.get( i ).update( deltaTime );

		for ( int i = 0; i < children.size; i++ )
			children.get( i ).update( deltaTime );
	}

	//render both bags
	public void render( SpriteBatch batch ) {
		children.sort( );

		for ( int i = 0; i < components.size; i++ )
			if( components.get( i ).getRenders( ) )
				components.get( i ).render( batch );

		for ( int i = 0; i < children.size; i++ )
			children.get( i ).render( batch );
	}


	public void setParent( GameObject parent ) {
		this.parent = parent;
	}

	public GameObject getParent() {
		return parent;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}


	//World position would be fine if there was some higher scene element to control all elements, at this point, its
		// not used
	public Vector2 getWorldPosition() {
		return worldPosition;
	}

	public void setWorldPosition( float x, float y ) {
		worldPosition.set( x, y );
	}

	public void setWorldPosition( Vector2 pos ) {
		worldPosition.set( pos );
	}

	public Vector2 getLocalPosition() {
		Vector2 localPosition = new Vector2( worldPosition );
		localPosition.add( localOffset );
		return localPosition;
	}

	public void setLocalPosition( Vector2 pos ) {
		localOffset.set( pos );
	}

	public void setLocalPosition( float x, float y ) {
		this.setLocalPosition( new Vector2( x, y ) );
	}

	//Determine the rendering order on a per-GameObject basis
	public int getzLevel() {
		return zLevel;
	}

	public void setzLevel( int newLevel ) {
		this.zLevel = newLevel;
	}

	private void updateLocalPosition() {
		if( parent != null ) {
			worldPosition.set( parent.getLocalPosition( ) );
		}
	}

	//-------------------------------------------------------------------------------------------------------
	//Children Methods

	public int addChild( GameObject child ) {
		children.add( child );
		child.setParent( this );
		return children.indexOf( child, true );
	}

	public boolean removeChild( GameObject child ) {
		boolean success = children.contains( child, true );

		if( success )
			children.removeValue( child, true );

		return success;
	}

	// Searches current children then children of children
	public <T extends GameObject> T findChildByType( Class<? extends GameObject> classOfInterest ) {
		T go = getChildByType( classOfInterest );

		if( go == null )
			go = getChildOfChildByType( classOfInterest );

		return go;
	}

	//searches only this bag (Shallow search)
	public <T extends GameObject> T getChildByType( Class<? extends GameObject> classOfInterest ) {
		T go = null;

		for ( GameObject c : children ) {
			if( classOfInterest.isInstance( c ) ) {
				go = (T) c;
				break;
			}
		}

		return go;
	}

	//TODO: Isolation Test
	//ignores this bag and searches children's bags (deep search)
	public <T extends GameObject> T getChildOfChildByType( Class<? extends GameObject> classOfInterest ) {
		T go = null;

		for ( GameObject c : children ) {
			go = c.findChildByType( classOfInterest );
			if( go != null ) break;
		}

		return go;
	}

	//array of all children that match the type, shallow and deep
	public <T extends GameObject> Array<T> findAllChildrenByType( Class<? extends GameObject> classOfInterest ) {
		Array<T> kids = new Array<T>( );

		for ( GameObject c : children )
			if( classOfInterest.isInstance( c ) ) kids.add( (T) c );

		for ( GameObject c : children ) {
			Array<T> moreKids = c.findAllChildrenByType( classOfInterest );
			if( moreKids.size > 0 ) kids.addAll( moreKids );
		}

		return kids;
	}

	public Array<GameObject> getChildren() {
		return children;
	}
	//-------------------------------------------------------------------------------------------------------


	//-------------------------------------------------------------------------------------------------------
	//Component Methods

	public int addComponent( Component c ) {
		components.add( c );
		c.setOwner( this );
		return components.indexOf( c, true );
	}

	public boolean removeComponent( Component c ) {
		boolean success = components.contains( c, true );

		if( success ) components.removeValue( c, true );

		return success;
	}

	//Search current components, then the components of children
	public <T extends Component> T findComponent( Class<? extends Component> classOfInterest ) {
		T obj = getComponent( classOfInterest );

		if( obj == null ) obj = getComponentInChildren( classOfInterest );

		return obj;
	}

	//search just this component bag (shallow)
	public <T extends Component> T getComponent( Class<? extends Component> classOfInterest ) {
		T obj = null;

		for ( Component c : components ) {
			if( classOfInterest.isInstance( c ) ) obj = (T) c;
			if( obj != null ) return obj;
		}

		return obj;
	}

	//TODO: Isolation Test
	//search just children's component bags (deep)
	public <T extends Component> T getComponentInChildren( Class<? extends Component> classOfInterest ) {
		T obj = null;

		for ( GameObject go : children ) {
			obj = go.getComponent( classOfInterest );
			if( obj != null ) return obj;
		}

		return obj;
	}

	//TODO: Isolation Test
	//array of all children that match the type, shallow and deep
	public <T extends Component> Array<T> findAllComponentsByType( Class<? extends Component> classOfInterest ) {
		Array<T> kids = new Array<T>( );

		for ( Component c : components )
			if( classOfInterest.isInstance( c ) ) kids.add( (T) c );

		for ( GameObject c : children ) {
			Array<T> moreKids = c.findAllComponentsByType( classOfInterest );
			if( moreKids.size > 0 ) kids.addAll( moreKids );
		}

		return kids;
	}

	public Array<Component> getComponents() {
		return components;
	}
	//-------------------------------------------------------------------------------------------------------
}
