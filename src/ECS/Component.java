/*
 * Copyright 2016 Andrew McGuiness
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at:
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ECS;


/***
 *		Component.java - A simple wrapper for complex behavior.  Can be referenced from the GameObject class using the
 *			getComponent( SomeComponent.class ) method.	Should be extended with unique behaviors.
 *
 * 		@author Andrew M.
 *  	@version 11/22/2016
 */

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class Component {
	protected boolean renders = true;
	protected boolean updates = true;

	protected GameObject owner;

	public Component( GameObject owner ){
		this.owner = owner;
	}

	public void update( float deltaTime ){
	}

	public void render( SpriteBatch batch ){
	}

	public boolean getRenders(){
		return renders;
	}

	public boolean getUpdates(){
		return updates;
	}

	public GameObject getOwner(){
		return owner;
	}

	public void setOwner( GameObject owner ){
		this.owner = owner;
	}
}
