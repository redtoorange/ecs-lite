/*
 * Copyright 2016 Andrew McGuiness
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at:
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package ECS;

/***
 *		PhysicsWorld.java - Container for a Box2D world that allows for the safe creation and destruction of bodies while
 *			the simulation is still running.  Implemented using the PhysicsComponent, which is just a wrapper for a Box2D
 *			body.
 *
 *        @author Andrew M.
 *  	@version 11/22/2016
 */

import ECS.ExampleComponents.PhysicsComponent;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

public class PhysicsWorld implements Disposable {
	private World world;
	private Box2DDebugRenderer debugRender;

	private Array<PhysicsComponent> waitingCreation = new Array<PhysicsComponent>( );
	private Array<PhysicsComponent> waitingDestruction = new Array<PhysicsComponent>( );

	public PhysicsWorld( Vector2 gravity, boolean canSleep ) {
		Box2D.init( );

		world = new World( gravity, canSleep );
		debugRender = new Box2DDebugRenderer( );
	}

	public void updateWorld( float delta, int velocityUpdates, int positionUpdates ) {
		world.step( delta, velocityUpdates, positionUpdates );

		if( waitingCreation.size > 0 ) {
			for ( PhysicsComponent c : waitingCreation )
				c.created( world );
			waitingCreation.clear( );
		}

		if( waitingDestruction.size > 0 ) {
			for ( PhysicsComponent c : waitingDestruction )
				c.destroyed( world );
			waitingDestruction.clear( );
		}
	}

	public void addInstance( PhysicsComponent comp ) {
		waitingCreation.add( comp );
	}

	public void destroyInstance( PhysicsComponent comp ) {
		waitingDestruction.add( comp );
	}

	public void debugRender( Camera camera ) {
		debugRender.render( world, camera.projection );
	}

	@Override
	public void dispose() {
		Array<Body> bodies = new Array<Body>( );
		world.getBodies( bodies );

		for ( int i = bodies.size - 1; i > 0; i++ )
			world.destroyBody( bodies.get( i ) );

		bodies.clear( );
	}
}
