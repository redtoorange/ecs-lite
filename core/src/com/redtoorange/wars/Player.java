/*
 * Copyright 2016 Andrew McGuiness
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at:
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package com.redtoorange.wars;


import ECS.ExampleComponents.*;
import ECS.GameObject;
import ECS.PhysicsWorld;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Player extends GameObject implements ClickComponentListener {
	private float speed = 1f;
	private boolean selected = false;

	//Caching references can save some time and remove the need for look ups
	private SpriteRenderer sr;
	private PhysicsComponent pc;
	private InputComponent ic;
	private ClickComponent cc;

	//Constructors should setup components for any gameObject
	public Player( PhysicsWorld world, Camera camera ) {
		super( "Player" );
		zLevel = 1;

		//visual representation
		sr = new SpriteRenderer( this, new Sprite( new Texture( "badlogic.jpg" ) ) );
		sr.getSprite( ).setSize( 1, 1 );
		addComponent( sr );

		//handle collision
		pc = new PhysicsComponent( this, world, 1, 1 );
		addComponent( pc );

		//handle keyboard input
		ic = new InputComponent( this );
		addComponent( ic );

		//handle mouse clicks
		cc = new ClickComponent( this, camera, 1, 1 );
		addComponent( cc );
	}

	@Override
	public void update( float deltaTime ) {
		super.update( deltaTime );

		Vector2 deltaImpulse = ic.getInput( );
		deltaImpulse.scl( speed );

		pc.addImpulse( deltaImpulse );

		if( selected ) sr.getSprite( ).setColor( Color.RED );
		else sr.getSprite( ).setColor( Color.WHITE );
	}

	private void select() {
		System.out.println( "selected " + name );
		selected = true;
	}

	private void deselect() {
		System.out.println( "deselected " + name );
		selected = false;
	}

	private void moveOrder(){
		pc.setLocation( cc.getLastClickLocation() );
	}



	//implementation of the ClickComponentListener
	@Override
	public void leftClick() {
		if( !selected && cc.mouseInsideBounds( ) ) select( );
		else if( selected ) deselect( );
	}

	@Override
	public void rightClick() {
		if( selected && !cc.mouseInsideBounds( ) ) {
			deselect( );
			System.out.println( "move order!" );
			moveOrder();
		}
		else if( selected && cc.mouseInsideBounds( ) ) deselect( );
	}
}
