/*
 * Copyright 2016 Andrew McGuiness
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at:
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 * for the specific language governing permissions and limitations under the License.
 */

package com.redtoorange.wars;

import ECS.ExampleComponents.ClickComponent;
import ECS.GameObject;
import ECS.PhysicsWorld;
import ECS.ExampleComponents.SpriteRenderer;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

public class MainGame extends ApplicationAdapter {
	OrthographicCamera camera;
	ExtendViewport viewport;
	SpriteBatch batch;

	Sprite sprite;
	float speed = 5f;

	private GameObject root;
	private PhysicsWorld world;

	@Override
	public void create() {
		world = new PhysicsWorld( new Vector2( 0, 0 ), true );

		camera = new OrthographicCamera( 10, 9 );
		viewport = new ExtendViewport( 10, 9, 10, 9, camera );

		sprite = new Sprite( new Texture( "badlogic.jpg" ) );
		sprite.setSize( 1, 1 );

		//Use of a toplevel entity is require for cascade updates, but you can have independent Entities that are updated
		// on their own
		root = new FakeGameObject( "root" );

		//Random test objects
		GameObject go = new FakeGameObject( "sprite gameObject" );
		go.setLocalPosition( 1, 1 );
		go.addComponent( new SpriteRenderer( go, new Sprite( sprite ) ) );
		go.addComponent( new ClickComponent( go, camera, 1, 1 ) );
		root.addChild( go );

		go = new FakeGameObject( "fake gameObject" );
		go.setLocalPosition( 2, 2 );
		go.setzLevel( 2 );
		go.addComponent( new SpriteRenderer( go, new Sprite( sprite ) ) );
		root.addChild( go );

		go = new FakeGameObject( "also fake gameObject" );
		go.setLocalPosition( 3, 3 );
		go.addComponent( new SpriteRenderer( go, new Sprite( sprite ) ) );
		root.addChild( go );

		//An example of a player
		Player p = new Player( world, camera );
		root.addChild( p );

		//added to player to test local position tracking
		go = new FakeGameObject( " fake gameObject attached to player" );
		go.setLocalPosition( -1, -1 );
		go.addComponent( new SpriteRenderer( go, new Sprite( sprite ) ) );
		p.addChild( go );


		batch = new SpriteBatch( );

		//Some test printing to determine if the component/entity lookup is working
		Array<FakeGameObject> fakes = root.findAllChildrenByType( FakeGameObject.class );
		Array<FakeGameObjectAlso> fakesAlso = root.findAllChildrenByType( FakeGameObjectAlso.class );

		Array<SpriteRenderer> spriteRenderers = root.findAllComponentsByType( SpriteRenderer.class );

		if( fakes.size > 0 ) {
			System.out.println( " Found " + fakes.size + " fake gameObjects " );
		}
		if( fakesAlso.size > 0 ) {
			System.out.println( " Found " + fakesAlso.size + " fake gameObjects also" );
		}
		if( spriteRenderers.size > 0 ) {
			System.out.println( " Found " + spriteRenderers.size + " sprite renderer" );
		}
	}

	@Override
	public void render() {
		world.updateWorld( Gdx.graphics.getDeltaTime( ), 6, 2 );

		clearScreen( );

		camera.update( );
		root.update( Gdx.graphics.getDeltaTime( ) );

		batch.setProjectionMatrix( camera.combined );
		batch.begin( );
		root.render( batch );
		batch.end( );

		world.debugRender( camera );
	}

	private void clearScreen() {
		Gdx.gl.glClearColor( 1, 0, 0, 1 );
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
	}

}
